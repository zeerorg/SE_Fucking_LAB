cmake_minimum_required(VERSION 3.8)
project(SE_Lab)

set(CMAKE_CXX_STANDARD 11)

set(SOURCE_FILES main.cpp func_pt.cpp)
add_executable(SE_Lab ${SOURCE_FILES})